import os
os.environ['CUDA_VISIBLE_DEVICES'] = str(0)

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

import json
from math import ceil
from slackclient import SlackClient

from keras.optimizers  import Adam, SGD
from keras.callbacks import ModelCheckpoint, LearningRateScheduler, TensorBoard, Callback, ReduceLROnPlateau
from keras import backend as K
from keras.models import load_model

from models.keras_ssd300 import ssd_300
from keras_loss_function.keras_ssd_loss import SSDLoss
from keras_layers.keras_layer_AnchorBoxes import AnchorBoxes
from keras_layers.keras_layer_DecodeDetections import DecodeDetections
from keras_layers.keras_layer_DecodeDetectionsFast import DecodeDetectionsFast
from keras_layers.keras_layer_L2Normalization import L2Normalization

from ssd_encoder_decoder.ssd_input_encoder import SSDInputEncoder
from ssd_encoder_decoder.ssd_output_decoder import decode_detections, decode_detections_fast

from data_generator.object_detection_2d_data_generator import DataGenerator
from data_generator.object_detection_2d_geometric_ops import Resize
from data_generator.object_detection_2d_photometric_ops import ConvertTo3Channels
from data_generator.data_augmentation_chain_original_ssd import SSDDataAugmentation
from data_generator.object_detection_2d_misc_utils import apply_inverse_transforms

image_dir = "image_data/train/"
annotation_file = 'tl_mapping_filter.csv'
train_csv = 'tl_train_filter.csv'

classes = {l.split(',')[0]: int(l.split(',')[1]) for l in 
           open(annotation_file).readlines()}

img_height = 300
img_width = 300
img_channels = 3
mean_color = [123, 117, 104]
swap_channels = [2, 1, 0]
n_classes = len(classes)
scales_pascal = [0.1, 0.2, 0.37, 0.54, 0.71, 0.88, 1.05]
scales = scales_pascal
aspect_ratios = [[1.0, 2.0, 0.5],
                 [1.0, 2.0, 0.5, 3.0, 1.0/3.0],
                 [1.0, 2.0, 0.5, 3.0, 1.0/3.0],
                 [1.0, 2.0, 0.5, 3.0, 1.0/3.0],
                 [1.0, 2.0, 0.5],
                 [1.0, 2.0, 0.5]]
two_boxes_for_ar1 = True
steps = [8, 16, 32, 64, 100, 300]
offsets = [0.5, 0.5, 0.5, 0.5, 0.5, 0.5]
clip_boxes = False
variances = [0.1, 0.1, 0.2, 0.2]
normalize_coords = True

K.clear_session()
model = ssd_300(image_size=(img_height, img_width, img_channels),
                n_classes=11,
                mode='training',
                l2_regularization=0.0005,
                scales=scales,
                aspect_ratios_per_layer=aspect_ratios,
                two_boxes_for_ar1=two_boxes_for_ar1,
                steps=steps,
                offsets=offsets,
                clip_boxes=clip_boxes,
                variances=variances,
                normalize_coords=normalize_coords,
                subtract_mean=mean_color,
                swap_channels=swap_channels)
model.load_weights("weights/VGG_ILSVRC_16_layers_fc_reduced.h5", by_name=True)

# sgd = SGD(lr=0.001, momentum=0.9, decay=0.0, nesterov=True)
adam = Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
ssd_loss = SSDLoss(neg_pos_ratio=3, alpha=1.0)
model.compile(optimizer=adam, loss=ssd_loss.compute_loss)

train_dataset = DataGenerator(load_images_into_memory=False, hdf5_dataset_path=None,
                             labels_output_format=('class_id', 'xmin', 'ymin', 'xmax', 'ymax'))
test_dataset = DataGenerator(load_images_into_memory=False, hdf5_dataset_path=None,
                           labels_output_format=('class_id', 'xmin', 'ymin', 'xmax', 'ymax'))

train_csv = 'tl_train_filter.csv'
test_csv = 'tl_test_filter.csv'

train_dataset.parse_csv(image_dir, train_csv, 
                 input_format=['image_name', 'xmin', 'ymin', 'xmax', 'ymax', 'class_id'])
test_dataset.parse_csv(image_dir, test_csv, 
                 input_format=['image_name', 'xmin', 'ymin', 'xmax', 'ymax', 'class_id'])

batch_size = 16

convert_to_3_channels = ConvertTo3Channels()
resize = Resize(height=img_height, width=img_width)

ssd_data_augmentation = SSDDataAugmentation(img_height=img_height,
                                            img_width=img_width,
                                            background=mean_color)

predictor_sizes = [model.get_layer('conv4_3_norm_mbox_conf').output_shape[1:3],
                   model.get_layer('fc7_mbox_conf').output_shape[1:3],
                   model.get_layer('conv6_2_mbox_conf').output_shape[1:3],
                   model.get_layer('conv7_2_mbox_conf').output_shape[1:3],
                   model.get_layer('conv8_2_mbox_conf').output_shape[1:3],
                   model.get_layer('conv9_2_mbox_conf').output_shape[1:3]]

ssd_input_encoder = SSDInputEncoder(img_height=img_height,
                                    img_width=img_width,
                                    n_classes=n_classes,
                                    predictor_sizes=predictor_sizes,
                                    scales=scales,
                                    aspect_ratios_per_layer=aspect_ratios,
                                    two_boxes_for_ar1=two_boxes_for_ar1,
                                    steps=steps,
                                    offsets=offsets,
                                    clip_boxes=clip_boxes,
                                    variances=variances,
                                    matching_type='multi',
                                    pos_iou_threshold=0.5,
                                    neg_iou_limit=0.5,
                                    normalize_coords=normalize_coords)

train_generator = train_dataset.generate(batch_size=batch_size,
                                         shuffle=True,
                                         transformations=[ssd_data_augmentation],
                                         label_encoder=ssd_input_encoder,
                                         returns={'processed_images',
                                                  'encoded_labels'},
                                         keep_images_without_gt=False)

test_generator = test_dataset.generate(batch_size=batch_size,
                                     shuffle=False,
                                     transformations=[convert_to_3_channels,
                                                      resize],
                                     label_encoder=ssd_input_encoder,
                                     returns={'processed_images',
                                              'encoded_labels'},
                                     keep_images_without_gt=False)

train_dataset_size = train_dataset.get_dataset_size()
test_dataset_size   = test_dataset.get_dataset_size()
print("Number of images in the training dataset:\t{:>6}".format(train_dataset_size))
print("Number of images in the testing dataset:\t{:>6}".format(test_dataset_size))

def lr_schedule(epoch):
    if epoch < 60:
        return 0.001
    elif epoch < 80:
        return 0.0005
    else:
        return 0.0001
    
model_checkpoint = ModelCheckpoint(filepath='snapshots/{epoch:02d}_val_loss-{val_loss:.4f}.h5',
                                   monitor='val_loss',
                                   verbose=1,
                                   save_best_only=True,
                                   save_weights_only=False,
                                   mode='auto',
                                   period=1)
board = TensorBoard(log_dir="logs_adam_16")
learning_rate_scheduler = LearningRateScheduler(schedule=lr_schedule,
                                                verbose=1)
plateau = ReduceLROnPlateau(monitor='val_loss', factor=np.sqrt(0.1), cooldown=0, patience=5, min_lr=0.5e-6)

class SlackAPI:

    def __init__(self, slack_token, username='ML_Integration', title='SSD 300 Model',
                 channels=['@alex']):
        self.client = SlackClient(slack_token)
        self.username = username
        self.title = title
        self.channels = channels

    def bulk_message(self, message, success):
        for channel in self.channels:
            success_color = 'good' if success else 'danger'
            attachments = {'color': success_color}

            fields = list()
            fields.append({'title': self.title, 'value': message, 'short': False})

            attachments['fields'] = fields
            attachments = json.dumps([attachments])

            self.client.api_call('chat.postMessage',
                                 text=self.title,
                                 attachments=attachments,
                                 channel=channel,
                                 username=self.username)

            return True
        
class SlackBot(Callback):
    slackbot = SlackAPI(slack_token=str("xoxp-75699467875-399945568823-424727277424-8b9d489b0caec0b0208f427e39e73c00"))
    
    def on_train_begin(self, logs={}):
        return 
 
    def on_train_end(self, logs={}):
        return
 
    def on_epoch_begin(self, epoch, logs={}):
        return
 
    def on_epoch_end(self, epoch, logs={}):
        loss = logs.get('loss')
        val_loss = logs.get('val_loss')
        
        self.slackbot.bulk_message(message="{}, loss: {:.4f}, val_loss: {:.4f}".format(
            epoch, loss, val_loss),success=True)
        
        return
 
    def on_batch_begin(self, batch, logs={}):
        return
 
    def on_batch_end(self, batch, logs={}):
        return
    
initial_epoch   = 0
final_epoch     = 120
steps_per_epoch = train_dataset_size // batch_size
bot = SlackBot()

history = model.fit_generator(generator=train_generator,
                              steps_per_epoch=steps_per_epoch,
                              epochs=final_epoch,
                              callbacks=[model_checkpoint, board, 
                                         plateau, bot],
                              validation_data=test_generator,
                              validation_steps=ceil(test_dataset_size/batch_size),
                              initial_epoch=initial_epoch,
                             verbose=0)
